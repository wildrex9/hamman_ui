﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsMenuUIManager : MonoBehaviour
{
	public Animator optionsPanel;
	public Animator descriptionsPanel;
	
	public Animator displayPanel;
	public Animator audioPanel;
	public Animator languagePanel;
	public Animator controlsPanel;
	
	public Animator closeButton;
	
	public CanvasGroup displayText;
	public CanvasGroup audioText;
	public CanvasGroup languageText;
	public CanvasGroup controlsText;
	public CanvasGroup settingsPanel;
	
	public Text displayDesc;
	public Text audioDesc;
	public Text languageDesc;
	public Text controlsDesc;
	
	public float speed = 1f;
	public float settingsMenuSpeed = 1f;
	
	bool displayFadeIn;
	bool audioFadeIn;
	bool controlsFadeIn;
	bool languageFadeIn;
	bool settingsFadeOut;
	
	//All this is for fading stuff in and out
	public void Start()
	{
		displayFadeIn = false;
		audioFadeIn = false;
		controlsFadeIn = false;
		languageFadeIn = false;
		settingsFadeOut = false;
		
		
		displayText.alpha = 0f;
		audioText.alpha = 0f;
		languageText.alpha = 0f;
		controlsText.alpha = 0f;
	}
	
	public void Update()
	{
		if (displayFadeIn)
		{
			displayText.alpha = Mathf.MoveTowards(displayText.alpha, 1f, Time.deltaTime * speed);
		}else
			displayText.alpha = Mathf.MoveTowards(displayText.alpha, 0f, Time.deltaTime * speed);
		
		
		if (audioFadeIn)
		{
			audioText.alpha = Mathf.MoveTowards(audioText.alpha, 1f, Time.deltaTime * speed);
		}else
			audioText.alpha = Mathf.MoveTowards(audioText.alpha, 0f, Time.deltaTime * speed);
		
		
		if (controlsFadeIn)
		{
			controlsText.alpha = Mathf.MoveTowards(controlsText.alpha, 1f, Time.deltaTime * speed);
		}else
			controlsText.alpha = Mathf.MoveTowards(controlsText.alpha, 0f, Time.deltaTime * speed);
		
		
		if (languageFadeIn)
		{
			languageText.alpha = Mathf.MoveTowards(languageText.alpha, 1f, Time.deltaTime * speed);
		}else
			languageText.alpha = Mathf.MoveTowards(languageText.alpha, 0f, Time.deltaTime * speed);
		
		if (settingsFadeOut)
		{
			settingsPanel.alpha = Mathf.MoveTowards(settingsPanel.alpha, 0f, Time.deltaTime * settingsMenuSpeed);
		}else
			settingsPanel.alpha = Mathf.MoveTowards(settingsPanel.alpha, 1f, Time.deltaTime * speed);
	}
	
	//Display fading stuff
	public void EnterDisplay(string toDisplay)
	{
		displayFadeIn = true;
		displayDesc.text = toDisplay;
	}

	public void ExitDisplay()
	{
		displayFadeIn = false;
	}
	
	//Audio fading stuff
	public void EnterAudio(string toDisplay)
	{
		audioFadeIn = true;
		audioDesc.text = toDisplay;
	}
	
	public void ExitAudio()
	{
		audioFadeIn = false;
	}
	
	//Controls fading stuff
	public void EnterControls (string toDisplay)
	{
		controlsFadeIn = true;
		controlsDesc.text = toDisplay;
	}
	
	public void ExitControls()
	{
		controlsFadeIn = false;
	}
	
	//Language fading stuff
	public void EnterLanguage (string toDisplay)
	{
		languageFadeIn = true;
		languageDesc.text = toDisplay;
	}
	
	public void ExitLanguage()
	{
		languageFadeIn = false;
	}
	
	//Animations for sliding things out/in
	public void openDisplayOptions()
	{
		optionsPanel.SetBool("IsHidden", true);
		descriptionsPanel.SetBool("IsHidden", true);
		closeButton.SetBool("IsHidden", true);
		
		displayPanel.SetBool("IsHidden", false);
	}
	
	public void openAudioOptions()
	{
		optionsPanel.SetBool("IsHidden", true);
		descriptionsPanel.SetBool("IsHidden", true);
		closeButton.SetBool("IsHidden", true);
		
		audioPanel.SetBool("IsHidden", false);
	}
	
	public void openLanguageOptions()
	{
		optionsPanel.SetBool("IsHidden", true);
		descriptionsPanel.SetBool("IsHidden", true);
		closeButton.SetBool("IsHidden", true);
		
		languagePanel.SetBool("IsHidden", false);
	}
	
	public void openControlsOptions()
	{
		optionsPanel.SetBool("IsHidden", true);
		descriptionsPanel.SetBool("IsHidden", true);
		closeButton.SetBool("IsHidden", true);
		
		controlsPanel.SetBool("IsHidden", false);
	}
	
	//Animations for sliding things in/out
	public void closeDisplayOptions()
	{
		optionsPanel.SetBool("IsHidden", false);
		descriptionsPanel.SetBool("IsHidden", false);
		closeButton.SetBool("IsHidden", false);
		
		displayPanel.SetBool("IsHidden", true);
	}
	
	public void closeAudioOptions()
	{
		optionsPanel.SetBool("IsHidden", false);
		descriptionsPanel.SetBool("IsHidden", false);
		closeButton.SetBool("IsHidden", false);
		
		audioPanel.SetBool("IsHidden", true);
	}
	
	public void closeLanguageOptions()
	{
		optionsPanel.SetBool("IsHidden", false);
		descriptionsPanel.SetBool("IsHidden", false);
		closeButton.SetBool("IsHidden", false);
		
		languagePanel.SetBool("IsHidden", true);
	}
	
	public void closeControlsOptions()
	{
		optionsPanel.SetBool("IsHidden", false);
		descriptionsPanel.SetBool("IsHidden", false);
		closeButton.SetBool("IsHidden", false);
		
		controlsPanel.SetBool("IsHidden", true);
	}
	
	//To theoratically close everything
	public void closeSettingsMenu()
	{
		optionsPanel.SetBool("IsHidden", true);
		descriptionsPanel.SetBool("IsHidden", true);
		
		closeButton.SetBool("IsHidden", true);
		settingsFadeOut = true;
	}
}
